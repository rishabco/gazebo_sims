#!/usr/bin/env python

import rospy
from geometry_msgs.msg import Twist

def get_cmd():
    value = raw_input('a:left, w: forward, d: right, s: backward, z: up, c:down ')
    switcher = {'a':(1,0,0,0,0,0),'d':(-1,0,0,0,0,0),'w':(0,1,0,0,0,0),'s':(0,-1,0,0,0,0),'.':(0,0,0,0,0,0),'z':(0,0,1,0,0,0),'c':(0,0,-1,0,0,0)}
    val = switcher[value]
    print val
    t = Twist()
    t.linear.x, t.linear.y, t.linear.z, t.angular.x, t.angular.y, t.angular.z = val
    #print twist.linear.x, twist.linear.y, twist.linear.z, twist.angular.z
    # flag = 0
    return t

def keyboard():
    pub = rospy.Publisher('/cmd_vel',Twist, queue_size=10)
    rospy.init_node('hector_keyboard_teleop')
    rate = rospy.Rate(1) 
    while not rospy.is_shutdown():
        twist = get_cmd()
        pub.publish(twist)
        rate.sleep()

if __name__ == "__main__":
    keyboard()