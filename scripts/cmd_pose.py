#!/usr/bin/env python

import rospy
from geometry_msgs.msg import PoseStamped

pose_list = [()]

def command_pose():
    msg = PoseStamped()
    msg.pose.position = (5,5,5)
    msg.pose.orientation = (0,0,0,1)
    # msg.time = rospy.now

    return msg

if __name__ == "__main__":
    rospy.init_node('pose_commander')
    pub = rospy.Publisher('/command/pose',PoseStamped,queue_size=10)

    msg = command_pose()
    pub.publish(msg)