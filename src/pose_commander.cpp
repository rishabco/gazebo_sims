/*
* UAV Pose Commander
* Author: Ian Loefgren
* Last Modified: 11/8/2017
*
* Command UAV to poses using PoseStamped messages. Poses are computed using
* trig such that the UAV orbits the origin. UGV position is obtained via
* transform data.
*/

#include <ros/ros.h>
#include <math.h>
#include <geometry_msgs/PoseStamped.h>
#include <tf/transform_listener.h>

#define PI 3.141592653589

geometry_msgs::PoseStamped get_next_pose(int count,float x,float y)
{
    geometry_msgs::PoseStamped msg;
    msg.header.stamp = ros::Time::now(); // get current time for message
    msg.header.frame_id = "uav/world"; // frame to work in for pose command, <>TODO: dynamically prepend namespace instead of hardcoding
    msg.pose.position.x = 4*cos(count*PI/180)+x; // current UGV pos plus orbit
    msg.pose.position.y = 4*sin(count*PI/180)+y;
    msg.pose.position.z = 3;
    // <>TODO: orient UAV to always point at UGV using quaternions
    msg.pose.orientation.x = 0;
    msg.pose.orientation.y = 0;
    msg.pose.orientation.z = 0;
    msg.pose.orientation.w = 1;
    return msg;
}

int main(int argc, char ** argv)
{
    // Initialize ROS node with name "pose_commander"
    ros::init(argc, argv, "pose_commander");
    // Create handle to acess node functionality
    ros::NodeHandle nh;
    // create publisher on node to UAV command pose topic with 100 size queue
    ros::Publisher pose_pub = nh.advertise<geometry_msgs::PoseStamped>("/uav/command/pose",100);
    // create transform listener to grab tf data
    tf::TransformListener listener;

    ros::Rate loop_rate(5); // set rate for update loop [Hz]

    int count = 0;

    // ros::ok()==0 occurs when node gets SIGTERM interrupt or similar
    while(ros::ok())
    {
        // get UGV current position from transform data -> no namespace on UGV nodes currently
        ros::Time current_time;
        tf::StampedTransform transform;
        listener.waitForTransform("base_footprint","world",current_time,ros::Duration(3.0));
        listener.lookupTransform("base_footprint","world",current_time,transform);
        float x = transform.getOrigin().x();
        float y = transform.getOrigin().y();

        // get next UAV position to publish, and publish message
        geometry_msgs::PoseStamped msg = get_next_pose(count,x,y);
        count += 10;
        pose_pub.publish(msg);

        // block once
        ros::spinOnce();
        // sleep for time defined by loop rate
        loop_rate.sleep();

        // increment count to define position in orbit (degrees)
        if(count >= 360)
            count = 0;
    }

    return 0;
}